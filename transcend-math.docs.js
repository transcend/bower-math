/**
 * @ngdoc overview
 * @name transcend.math
 * @description
 # Math Module
 The "Math" module provides mathematical tools for use in data analysis and processing.

 ## Installation
 The easiest way to use this, and other Transcend libraries, is to use [Bower.js](http://bower.io/). Run the following
 commands to install this package:

 ```
 bower install https://bitbucket.org/transcend/bower-math.git
 ```

 Other options:

 *   * Download the code at [http://code.tsstools.com/bower-lidar/get/master.zip](http://code.tsstools.com/bower-math/get/master.zip)
 *   * View the repository at [http://code.tsstools.com/bower-lidar](http://code.tsstools.com/bower-math)
 *   * If you have permissions, view the source code at [http://code.tsstools.com/web-common](http://code.tsstools.com/web-common/src/ee541a3bce850ae77f6d7948a00cc352b4f67b21/modules/math/?at=develop)

 When you have pulled down the code and included the files (and dependency files), simply add the module as a
 dependency to your application:

 ```
 angular.module('myModule', ['transcend.math']);
 ```

 ## To Do
 - Add unit tests.
 - Add better documentation.

 ## Project Goals
 - Keep this module size below 8kb minified
 */
/**
 * @ngdoc service
 * @name transcend.math.$math
 *
 * @description
 * Shortcut static class to be used to access common transcend functionality. This class
 * is essentially just a wrapper for the most common functionality.
 */
/**
       * @ngdoc method
       * @name transcend.math.$math:$math#evaluation
       * @methodOf transcend.math.$math
       * @description
       * Object which contains common evaluation functions for objects passed into the functions in this factory.
       * For example, the
       */
/**
       * @ngdoc method
       * @name transcend.math.$math:$math#getInlierMinMax
       * @methodOf transcend.math.$math
       * @description
       */
/**
       * @ngdoc method
       * @name transcend.math.$math:$math#findNestedOutliers
       * @methodOf transcend.math.$math
       * @description
       */
/**
       * @ngdoc method
       * @name transcend.math.$math:$math#getMedian
       * @methodOf transcend.math.$math
       * @description
       * Object which contains common evaluation functions for objects passed into the functions in this factory.
       * For example, the
       */
/**
       * @ngdoc method
       * @name transcend.math.$math:$math#getQuantiles
       * @methodOf transcend.math.$math
       * @description
       */
